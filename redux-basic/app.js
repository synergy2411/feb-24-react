const { configureStore } = require("@reduxjs/toolkit");

const initialState = {
  counter: 0,
};

// REDUCER
function reducerFn(state = initialState, action) {
  if (action.type === "INCREMENT") {
    return {
      counter: state.counter + 1,
    };
  } else if (action.type === "DECREMENT") {
    return {
      counter: state.counter - 1,
    };
  } else if (action.type === "ADD_10") {
    return (state.counter = state.counter + action.payload);
  }
  return state;
}

// STORE
const store = configureStore({
  reducer: reducerFn,
});

// SUBSCRIBE
store.subscribe(() => {
  console.log("STATE : ", store.getState());
});

// ACTIONS
store.dispatch({ type: "INCREMENT" });

// console.log("STATE AFTER INCREMENT : ", store.getState());

store.dispatch({ type: "DECREMENT" });

// console.log("STATE AFTER DECREMENT : ", store.getState());

store.dispatch({ type: "ADD_10", payload: 10 });

// console.log("STATE AFTER ADD_10 : ", store.getState());
