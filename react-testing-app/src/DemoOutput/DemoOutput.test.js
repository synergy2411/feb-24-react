import {
  findAllByRole,
  findByRole,
  getByRole,
  render,
  screen,
} from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import DemoOutput from "./DemoOutput";

describe("<DemoOutput />", () => {
  test("renders heading element", () => {
    render(<DemoOutput />);
    const headingElement = screen.getByText(/the demo output/i);
    expect(headingElement).toBeInTheDocument();
  });

  test("renders the paragraph element", () => {
    render(<DemoOutput />);
    const paragraphElement = screen.queryByText(
      /This paragraph will be displayed/,
      { exact: false }
    );
    expect(paragraphElement).not.toBeNull();
  });

  test("renders 'show value is true' when button is NOT clicked", () => {
    render(<DemoOutput />);
    const paragraphElement = screen.getByText(/show value is true/i);
    expect(paragraphElement).toBeInTheDocument();
  });

  test("renders 'show value is false' when the button is clicked", async () => {
    render(<DemoOutput />);

    const buttonElement = screen.getByText("Toggle");

    userEvent.click(buttonElement);

    const paragraphElement = await screen.findByText(/show value is false/i);

    expect(paragraphElement).toBeInTheDocument();
  });

  test("renders not 'show value is true' when the button is clicked", async () => {
    render(<DemoOutput />);

    const buttonElement = screen.getByText("Toggle");

    userEvent.click(buttonElement);

    const pElement = await screen.findByText(/show value is true/i);

    expect(pElement).not.toBeInTheDocument();
  });

  test("renders list of posts as list item", async () => {
    render(<DemoOutput />);
    const btnElement = screen.getByText("Fetch Posts");
    userEvent.click(btnElement);

    const listItems = await screen.findAllByRole("listitem");

    // expect(listItems).not.toHaveLength(0);
    expect(listItems).toHaveLength(100);
  });
});
