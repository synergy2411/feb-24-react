import { useEffect, useState } from "react";

function DemoOutput() {
  const [show, setShow] = useState(true);
  const [posts, setPosts] = useState([]);

  const fetchPostHandler = async () => {
    const response = await fetch("https://jsonplaceholder.typicode.com/posts");
    const posts = await response.json();
    setPosts(posts);
  };

  return (
    <>
      <h1>The Demo Output</h1>

      <p>This paragraph will be displayed</p>

      <button onClick={() => setShow(!show)}>Toggle</button>
      <button onClick={fetchPostHandler}>Fetch Posts</button>

      {show && <p>Show value is True</p>}
      {!show && <p>Show value is False</p>}

      <ul>
        {posts.map((post) => (
          <li key={post.id}>{post.title}</li>
        ))}
      </ul>
    </>
  );
}

export default DemoOutput;
