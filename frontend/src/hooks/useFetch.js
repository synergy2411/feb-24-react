import { useEffect, useState } from "react";

const useFetch = (endpoint) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(
          `https://jsonplaceholder.typicode.com/${endpoint}`
        );
        const data = await response.json();
        setData(data.map((post) => ({ id: post.id, title: post.title })));
      } catch (err) {
        console.log("ERROR ===> ", err);
      }
    };
    fetchData();
  }, []);

  return data;
};

export default useFetch;
