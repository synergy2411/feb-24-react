import { useState } from "react";

import ExpenseItem from "./ExpenseItem/ExpenseItem";
import AddExpense from "./AddExpense/AddExpense";
import ExpenseFilter from "./ExpenseFilter/ExpenseFilter";

let INITIAL_EXPENSES = [
  {
    id: "e001",
    title: "shopping",
    amount: 199,
    createdAt: new Date("Dec 12, 2022"),
  },
  {
    id: "e002",
    title: "grocery",
    amount: 39,
    createdAt: new Date("Aug 01, 2023"),
  },
  {
    id: "e003",
    title: "insurance",
    amount: 59,
    createdAt: new Date("Jan 18, 2024"),
  },
];

const Expenses = () => {
  const [expenses, setExpenses] = useState(INITIAL_EXPENSES);

  const [isShow, setIsShow] = useState(false);

  const [selectedYear, setSelectedYear] = useState("");

  const toggleHandler = () => setIsShow(!isShow);

  const closeFormHandler = () => setIsShow(false);

  const addExpenseHandler = (expense) => {
    setExpenses((prevExpense) => [expense, ...prevExpense]);
    closeFormHandler();
  };

  const deleteExpenseHandler = (expenseId) =>
    setExpenses((prevExp) => prevExp.filter((exp) => exp.id !== expenseId));

  const onYearSelect = (selectedYear) => {
    console.log(selectedYear);
    setSelectedYear(selectedYear);
  };

  let filteredExpenses;
  if (selectedYear !== "") {
    filteredExpenses = expenses.filter(
      (exp) => exp.createdAt.getFullYear() === Number(selectedYear)
    );
  } else {
    filteredExpenses = [...expenses];
  }

  return (
    <>
      <h1 className="text-center">My Expenses</h1>

      <div className="row mb-4">
        <div className="col-4 offset-4">
          <div className="d-grid">
            <button className="btn btn-dark" onClick={toggleHandler}>
              Add Expense
            </button>
          </div>
        </div>
        <div className="col-4">
          <ExpenseFilter
            selectedYear={selectedYear}
            onYearSelect={onYearSelect}
          />
        </div>
      </div>
      {isShow && (
        <AddExpense onAdd={addExpenseHandler} onClose={closeFormHandler} />
      )}

      <div className="row">
        {filteredExpenses.map((expense) => (
          <ExpenseItem
            key={expense.id}
            id={expense.id}
            title={expense.title}
            amount={expense.amount}
            createdAt={expense.createdAt}
            onDelete={deleteExpenseHandler}
          />
        ))}
      </div>
    </>
  );
};

export default Expenses;
