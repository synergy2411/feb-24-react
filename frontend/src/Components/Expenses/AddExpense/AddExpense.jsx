import { useState } from "react";
import { v4 } from "uuid";

const AddExpense = ({ onClose, onAdd }) => {
  const [enteredTitle, setEnteredTitle] = useState("");
  const [enteredAmount, setEnteredAmount] = useState("");
  const [enteredCreatedAt, setEnteredCreatedAt] = useState("");

  const titleChangeHandler = (e) => setEnteredTitle(e.target.value);

  const amountChangeHandler = (e) => setEnteredAmount(e.target.value);

  const createdAtChangeHandler = (e) => setEnteredCreatedAt(e.target.value);

  const submitHandler = (event) => {
    event.preventDefault();
    const newExpense = {
      id: v4(),
      title: enteredTitle,
      amount: Number(enteredAmount),
      createdAt: new Date(enteredCreatedAt),
    };
    onAdd(newExpense);
  };

  return (
    <div>
      <form onSubmit={submitHandler}>
        {/* title */}
        <div className="form-floating mb-3">
          <input
            type="text"
            className="form-control"
            name="title"
            id="title"
            onChange={titleChangeHandler}
            value={enteredTitle}
          />
          <label htmlFor="title">Title</label>
        </div>

        {/* amount */}
        <div className="form-floating mb-3">
          <input
            type="number"
            className="form-control"
            name="amount"
            id="amount"
            placeholder=""
            min="0.5"
            step="0.5"
            value={enteredAmount}
            onChange={amountChangeHandler}
          />
          <label htmlFor="amount">Amount</label>
        </div>

        {/* createdAt */}

        <div className="form-floating mb-3">
          <input
            type="date"
            min="2021-04-01"
            max="2024-03-31"
            className="form-control"
            name="createdAt"
            id="createdAt"
            placeholder=""
            value={enteredCreatedAt}
            onChange={createdAtChangeHandler}
          />
          <label htmlFor="createdAt">Date</label>
        </div>

        {/* buttons */}
        <div className="row">
          <div className="col">
            <div className="d-grid">
              <button className="btn btn-primary" type="submit">
                Add
              </button>
            </div>
          </div>
          <div className="col">
            <div className="d-grid">
              <button className="btn btn-secondary" onClick={onClose}>
                Close
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
};

export default AddExpense;
