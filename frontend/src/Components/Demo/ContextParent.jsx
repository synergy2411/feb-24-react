import { useContext } from "react";
import AuthContext from "../../context/auth-context";
import ContextChild from "./ContextChild";

const ContextParent = () => {
  const context = useContext(AuthContext);
  return (
    <>
      <h1>Context Parent Component</h1>
      <button
        onClick={() => context.setIsLoggedIn(!context.isLoggedIn)}
        className="btn btn-primary"
      >
        {context.isLoggedIn ? "Logout" : "Login"}
      </button>
      <hr />
      <ContextChild />
    </>
  );
};

export default ContextParent;
