import { useRef, useState } from "react";

const RefDemo = () => {
  console.log("RefDemo Rendered");
  const inputRef = useRef();
  const [enteredPassword, setEnteredPassword] = useState("");

  const submitHandler = (e) => {
    e.preventDefault();
    console.log("Email  :", inputRef.current.value);
    console.log("Password : ", enteredPassword);
  };

  return (
    <form>
      {/* email - Uncontrolled */}
      <input
        type="email"
        name="email"
        className="form-control"
        ref={inputRef}
      />
      {/* password - Controlled */}
      <input
        type="password"
        name="password"
        value={enteredPassword}
        onChange={(e) => setEnteredPassword(e.target.value)}
      />
      <button onClick={submitHandler}>Submit</button>
    </form>
  );
};

export default RefDemo;
