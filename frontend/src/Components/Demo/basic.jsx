import React, { Fragment } from "react";

function Basic() {
  let username = "John Doe";

  let userage = 32;
  let isAdmin = false;

  let employee = {
    email: "john@test.com",
  };

  let dob = new Date("Dec 12, 1985");

  return (
    <>
      <div>
        <h1>The Basic Demo</h1>
        <p className="user-email">Email : {employee.email}</p>

        <p>User Name : {username}</p>
        <p>User Age : {userage}</p>
        <p>This user is {isAdmin ? "" : "NOT"} Admin</p>
      </div>
      <div>
        <h1>The Second Heading</h1>
      </div>
    </>
  );
}

export default Basic;
