import AuthContext from "../../context/auth-context";

const ContextChild = () => {
  return (
    <AuthContext.Consumer>
      {(context) => {
        return (
          <>
            <h1>Context Child Component</h1>

            {context.isLoggedIn && <p>The user is logged in.</p>}
            {!context.isLoggedIn && <p>The user is NOT logged in.</p>}
          </>
        );
      }}
    </AuthContext.Consumer>
  );
};

export default ContextChild;
