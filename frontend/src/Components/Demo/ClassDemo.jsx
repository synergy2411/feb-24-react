import { Component } from "react";

class ClassDemo extends Component {
  state = {
    counter: 0,
    todos: [],
  };

  constructor() {
    super();
    console.log("CONSTRUCTOR");
  }

  componentDidMount() {
    console.log("COMPONENT DID MOUNT");
    const fetchTodos = async () => {
      const response = await fetch(
        "https://jsonplaceholder.typicode.com/todos"
      );
      const todos = await response.json();
      this.setState({
        todos: todos,
      });
    };
    fetchTodos();
  }

  shouldComponentUpdate(nextProps, nextState) {
    const rnd = Math.round(Math.random() * 10);
    console.log("SHOULD COMPONENT UPDATE", rnd);
    console.log(nextState, nextProps);
    // return rnd > 4;
    return true;
  }

  componentDidUpdate() {
    console.log("COMPONENT DID UPDATE");
  }

  componentWillUnmount() {
    console.log("COMPONENT WILL UNMOUNT");
  }

  onIncrease() {
    this.setState({ counter: this.state.counter + 1 });
  }

  render() {
    console.log("RENDER");
    return (
      <>
        <h1>Class Based Component Loaded</h1>
        <button
          className="btn btn-primary"
          onClick={this.onIncrease.bind(this)}
        >
          {this.state.counter}
        </button>
        <ul>
          {this.state.todos.map((todo) => (
            <li key={todo.id}>{todo.title}</li>
          ))}
        </ul>
      </>
    );
  }
}

export default ClassDemo;
