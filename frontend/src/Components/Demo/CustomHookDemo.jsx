import React from "react";
import useFetch from "../../hooks/useFetch";

export default function CustomHookDemo() {
  const data = useFetch("posts");
  console.log("DATA : ", data);
  return (
    <div>
      <ul>
        {data.length && data.map((todo) => <li key={todo.id}>{todo.title}</li>)}
      </ul>
    </div>
  );
}
