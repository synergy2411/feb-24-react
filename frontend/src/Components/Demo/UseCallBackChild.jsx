import { memo } from "react";

function UseCallBackChild({ toggle, callBackFn }) {
  console.log("CHILD");
  return (
    <>
      <h2>UseCallBack Child</h2>
      <button onClick={callBackFn}>Call Function</button>
      {toggle && <p>This content will toggle</p>}
    </>
  );
}

export default memo(UseCallBackChild);

// Memoization => to store

// when compare for referential equality
// prevProps.callBackFn === currProps.callBackFn

// prevProps.toggle === currProps.toggle
// true => don't re-render the component
// false => re-render the component
