import { useEffect, useState } from "react";

const UseEffectDemo = () => {
  const [counter, setCounter] = useState(0);

  const [toggle, setToggle] = useState(true);

  const [enteredSearchTerm, setEnteredSearchTerm] = useState("");

  const [repos, setRepos] = useState([]);

  //   useEffect(() => {
  //     console.log("Effect works!");
  //   });
  //   useEffect(() => {
  //     console.log("Effect works!");
  //   }, []);
  //   useEffect(() => {
  //     console.log("Effect works!");
  //   }, [toggle]);

  //   useEffect(() => {
  //     console.log("Effect works!");
  //     return () => {
  //       console.log("Clean up");
  //     };
  //   }, [toggle]);

  useEffect(() => {
    let timer;
    if (enteredSearchTerm.trim() !== "") {
      timer = setTimeout(() => {
        const fetchRepos = async () => {
          const response = await fetch(
            `https://api.github.com/users/${enteredSearchTerm}/repos`
          );
          const repos = await response.json();
          setRepos(repos);
        };
        fetchRepos();
      }, 1000);
    }
    return () => {
      clearTimeout(timer);
    };
  }, [enteredSearchTerm]);

  return (
    <>
      <h1>Use Effect Demo</h1>
      <button
        className="btn btn-primary"
        onClick={() => setCounter(counter + 1)}
      >
        {counter}
      </button>

      <button className="btn btn-secondary" onClick={() => setToggle(!toggle)}>
        Toggle
      </button>

      {toggle && <p>This paragrapg will toggle</p>}

      <input
        type="text"
        value={enteredSearchTerm}
        onChange={(e) => setEnteredSearchTerm(e.target.value)}
      />
      <ul>
        {repos.map((repo) => (
          <li key={repo.id}>{repo.full_name}</li>
        ))}
      </ul>
    </>
  );
};

export default UseEffectDemo;
