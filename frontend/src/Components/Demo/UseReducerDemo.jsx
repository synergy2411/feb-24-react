import { useReducer } from "react";

const reducerFn = (state, action) => {
  if (action.type === "INCREMENT") {
    return {
      ...state,
      counter: state.counter + 1,
    };
  } else if (action.type === "DECREMENT") {
    return {
      ...state,
      counter: state.counter - 1,
    };
  } else if (action.type === "ADD_COUNTER") {
    return {
      ...state,
      counter: state.counter + action.payload.value,
    };
  } else if (action.type === "STORE_RESULT") {
    //   complex logic to update state
    return {
      ...state,
      result: [action.payload, ...state.result],
    };
  }
  return state;
};

const UseReducerDemo = () => {
  const [state, dispatch] = useReducer(reducerFn, { counter: 0, result: [] });

  return (
    <>
      <h1>Use Reducer Demo Component</h1>
      <p className="display-4">Counter : {state.counter}</p>
      <button
        className="btn btn-primary"
        onClick={() => dispatch({ type: "INCREMENT" })}
      >
        Increase
      </button>
      <button
        className="btn btn-secondary"
        onClick={() => dispatch({ type: "DECREMENT" })}
      >
        Decrease
      </button>
      <button
        className="btn btn-warning"
        onClick={() =>
          dispatch({ type: "ADD_COUNTER", payload: { value: 10 } })
        }
      >
        Add - 10
      </button>
      <hr />

      <button
        className="btn btn-danger"
        onClick={() =>
          dispatch({ type: "STORE_RESULT", payload: state.counter })
        }
      >
        Store Result
      </button>

      <ul>
        {state.result.map((r) => (
          <li key={Math.random() * 1000}>{r}</li>
        ))}
      </ul>
    </>
  );
};

export default UseReducerDemo;
