import { useCallback, useMemo, useState } from "react";
import UseCallBackChild from "./UseCallBackChild";

export default function UseCallBackParent() {
  console.log("PARENT");
  const [toggle, setToggle] = useState(false);

  // xixi00123
  //   const callBackFn = useCallback(() => console.log("Callback Called"), []);

  const callBackFn = useMemo(() => () => console.log("Memoized Function"), []);

  let myFriends = ["Monica", "Joey", "Ross"];

  const friends = useMemo(() => {
    return myFriends;
  }, [myFriends]);

  return (
    <>
      <h1>UseCallBack Parent</h1>

      <button className="btn btn-primary" onClick={() => setToggle(!toggle)}>
        Toggle
      </button>

      <UseCallBackChild
        toggle={true}
        callBackFn={callBackFn}
        friends={friends}
      />
    </>
  );
}
