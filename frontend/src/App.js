import { useState } from "react";
import ContextParent from "./Components/Demo/ContextParent";
import AuthContext from "./context/auth-context";
import UseReducerDemo from "./Components/Demo/UseReducerDemo";
import UseCallBackParent from "./Components/Demo/UseCallBackParent";
import CustomHookDemo from "./Components/Demo/CustomHookDemo";

function App() {
  // const [isLoggedIn, setIsLoggedIn] = useState(false);

  return (
    <div className="container">
      <h1>App Loaded</h1>

      <CustomHookDemo />

      {/* <UseCallBackParent /> */}

      {/* <UseReducerDemo /> */}

      {/* <AuthContext.Provider value={{ isLoggedIn, setIsLoggedIn }}>
        <ContextParent />
      </AuthContext.Provider> */}
    </div>
  );
}

export default App;
