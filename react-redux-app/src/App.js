import { useSelector } from "react-redux";
import Auth from "./Components/Auth/Auth";
import { DotLoader } from "react-spinners";

import Todos from "./Components/Todos/Todos";

function App() {
  const { isLoading, errorMessage, token } = useSelector(
    (store) => store.authReducer
  );

  if (isLoading) {
    return <DotLoader size="180" color="#d63679" />;
  }

  if (errorMessage.trim() !== "") {
    return <h1>{errorMessage}</h1>;
  }

  return (
    <div className="container">
      {!token && <Auth />}
      {token && <Todos />}
    </div>
  );
}

export default App;
