import { useDispatch } from "react-redux";
import AddTodo from "./AddTodo";
import TodoList from "./TodoList";
import { authSignOut } from "../../store/slices/auth.slice";

function Todos() {
  const dispatch = useDispatch();
  return (
    <>
      <div className="row">
        <div className="col-10">
          <h1>My Todos</h1>
        </div>
        <div className="col-2">
          <button
            className="btn btn-outline-danger"
            onClick={() => dispatch(authSignOut())}
          >
            Logout
          </button>
        </div>
      </div>
      <AddTodo />
      <br />
      <TodoList />
    </>
  );
}

export default Todos;
