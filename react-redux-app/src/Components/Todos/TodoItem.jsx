import { useDispatch } from "react-redux";
import { deleteTodo } from "../../store/slices/todos.slice";

function TodoItem({ todo }) {
  const dispatch = useDispatch();

  return (
    <>
      <li className="list-group-item mb-3">
        {todo.label.toUpperCase()}
        <button
          onClick={() => dispatch(deleteTodo(todo.id))}
          className="btn btn-outline-danger float-end"
        >
          🗑️
        </button>
      </li>
    </>
  );
}

export default TodoItem;
