import { useState } from "react";
import { useDispatch } from "react-redux";
import { addTodo } from "../../store/slices/todos.slice";

function AddTodo() {
  const [enteredLabel, setEnteredLabel] = useState("");

  const dispatch = useDispatch();

  const labelChangeHandler = (e) => setEnteredLabel(e.target.value);

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(addTodo(enteredLabel));
    setEnteredLabel("");
  };

  return (
    <>
      <form onSubmit={submitHandler}>
        <div className="row">
          <div className="col-10">
            {/* label : input field */}
            <input
              type="text"
              className="form-control"
              name="label"
              id="label"
              placeholder="Enter new todo label"
              value={enteredLabel}
              onChange={labelChangeHandler}
            />
          </div>
          <div className="col-2">
            {/* button */}
            <div className="d-grid">
              <button className="btn btn-primary" type="submit">
                Add
              </button>
            </div>
          </div>
        </div>
      </form>
    </>
  );
}

export default AddTodo;
