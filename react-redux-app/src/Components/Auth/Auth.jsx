import { useRef } from "react";
import { useDispatch } from "react-redux";
import { authRegistration, authSignIn } from "../../store/slices/auth.slice";

function Auth() {
  const emailRef = useRef();
  const passwordRef = useRef();

  const dispatch = useDispatch();

  const registerHandler = (e) => {
    e.preventDefault();
    const user = {
      email: emailRef.current.value,
      password: passwordRef.current.value,
    };
    console.log(user);
    dispatch(authRegistration(user));
  };

  const loginHandler = (e) => {
    e.preventDefault();
    const user = {
      email: emailRef.current.value,
      password: passwordRef.current.value,
    };
    console.log(user);
    dispatch(authSignIn(user));
  };

  return (
    <>
      <h1>Auth Form</h1>
      <form>
        {/* email */}
        <div className="form-floating mb-3">
          <input
            type="text"
            className="form-control"
            name="email"
            id="email"
            placeholder=""
            ref={emailRef}
          />
          <label htmlFor="email">Email</label>
        </div>

        {/* password */}
        <div className="form-floating mb-3">
          <input
            type="password"
            className="form-control"
            name="password"
            id="password"
            placeholder=""
            ref={passwordRef}
          />
          <label htmlFor="password">Password</label>
        </div>

        {/* button */}
        <button
          className="btn btn-primary"
          type="button"
          onClick={registerHandler}
        >
          Register
        </button>

        <button className="btn btn-secondary" onClick={loginHandler}>
          Login
        </button>
      </form>
    </>
  );
}

export default Auth;
