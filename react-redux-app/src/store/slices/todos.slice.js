import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  todos: [
    { id: "t001", label: "grocery" },
    { id: "t002", label: "planting" },
    { id: "t003", label: "shopping" },
  ],
};

const todoSlice = createSlice({
  name: "todos",
  initialState,
  reducers: {
    addTodo: (state, action) => {
      state.todos.push({
        id: "t00" + (state.todos.length + 1),
        label: action.payload,
      });
    },
    deleteTodo: (state, action) => {
      const position = state.todos.findIndex(
        (todo) => todo.id === action.payload
      );
      state.todos.splice(position, 1);
    },
  },
});

export const { addTodo, deleteTodo } = todoSlice.actions;

export default todoSlice.reducer;
