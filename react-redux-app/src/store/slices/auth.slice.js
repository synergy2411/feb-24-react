import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  getAuth,
  signOut,
} from "firebase/auth";
import { initializeApp } from "firebase/app";

const app = initializeApp({
  apiKey: "AIzaSyAwHQC-y3Zk8F6bIxv82oN7Ly1huKk6kmE",
  authDomain: "syne-react-feb-24.firebaseapp.com",
});

const auth = getAuth(app);

export const authSignOut = createAsyncThunk(
  "auth/signOut",
  async (_, { rejectWithValue }) => {
    try {
      const response = await signOut(auth);
      console.log("SIGNED OUT", response);
      return true;
    } catch (err) {
      return rejectWithValue("Unable to signOut");
    }
  }
);

export const authSignIn = createAsyncThunk(
  "auth/sign-in",
  async ({ email, password }, { rejectWithValue }) => {
    try {
      const userCredentials = await signInWithEmailAndPassword(
        auth,
        email,
        password
      );
      const token = await userCredentials.user.getIdToken();
      return token;
    } catch (err) {
      return rejectWithValue("Unable to sign in for " + email);
    }
  }
);

export const authRegistration = createAsyncThunk(
  "auth/registration",
  async ({ email, password }, { rejectWithValue }) => {
    try {
      const userCredentails = await createUserWithEmailAndPassword(
        auth,
        email,
        password
      );
      const token = await userCredentails.user.getIdToken();
      console.log("TOKEN : ", token);
      return token;
    } catch (err) {
      return rejectWithValue("Unable to create the user.");
    }
  }
);

const initialState = {
  token: null,
  errorMessage: "",
  isLoading: false,
};
const authRegistrationSlice = createSlice({
  name: "auth/user-registration",
  initialState,
  extraReducers: (builder) => {
    //   User Registration
    builder.addCase(authRegistration.pending, (state, action) => {
      state.isLoading = true;
    });
    builder.addCase(authRegistration.fulfilled, (state, action) => {
      state.isLoading = false;
      console.log("FULFILLED CASE : ", action);
      state.token = action.payload;
    });
    builder.addCase(authRegistration.rejected, (state, action) => {
      state.isLoading = false;
      state.token = null;
      state.errorMessage = action.payload;
    });
    //   User Sign-In
    builder.addCase(authSignIn.pending, (state, action) => {
      state.isLoading = true;
    });
    builder.addCase(authSignIn.fulfilled, (state, action) => {
      state.isLoading = false;
      state.token = action.payload;
    });
    builder.addCase(authSignIn.rejected, (state, action) => {
      state.isLoading = false;
      console.log("REJECTED : ", action);
      state.token = null;
      state.errorMessage = action.payload;
    });

    //   User SignOut
    builder.addCase(authSignOut.pending, (state, action) => {
      state.isLoading = true;
    });
    builder.addCase(authSignOut.fulfilled, (state, action) => {
      if (action.payload) {
        state.isLoading = false;
        state.token = null;
        state.errorMessage = "";
      }
    });
    builder.addCase(authSignOut.rejected, (state, action) => {
      state.isLoading = false;
      state.token = null;
      console.log("PAYLOAD : ", action);
      state.errorMessage = action.payload;
    });
  },
});

export default authRegistrationSlice.reducer;
