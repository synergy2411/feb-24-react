import { configureStore } from "@reduxjs/toolkit";
import todoReducer from "./slices/todos.slice";
import authReducer from "./slices/auth.slice";

const store = configureStore({
  reducer: {
    todoReducer,
    authReducer,
  },
});

export default store;
