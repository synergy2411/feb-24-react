# Break timings

- Tea Break : 11:00AM (15mins)
- Lunch : 01:00PM (45mins)
- Tea Break : 3:30PM (15mins)

# JavaScript

- Scripting Language
- Single Threaded
- Non-blocking
- Asynchronous
- Dynamic Pages
- Functional Programming
- Dynamically Typed
- Compiled as well as Interpreted
- Both Client (Browser) as well as Server Side (Node Runtime Environment)

# Datatypes in JavaScript

- Primitive : Number, String, Boolean, BigInt, Symbol, undefined, null
- Reference : Object, Arrays, Functions, Date

# Various JavaScript Libraries / Framework

- React (30kb) : render the UI quickly and efficiently.

  > State Management : Redux Library
  > Form Validation : formik, react-hook-form, yup
  > Single Page App : React Router Library
  > XHR Calls : fetch API, axios

- jQuery : DOM manipulation, AJAX Calls, Animation
- Knockout : 2 way data binding, MVVM Pattern
- Bootstrap : JS Components and CSS Classes
- BackboneJS : MVC at client side
- React Native : Native Apps (Mobile Apps)
- \*Angular : SPA, Components, XHR Calls, Form Validation, DOM Manipulation, State Management, MVC Pattern, one way data binding
- \*NextJS : Server-Side React Code (SSR)
- \*VueJS : Emerging Community. 'Evan You'
- \*Ionic : for Mobile App, Native Apps, Hybrid Apps (Angular, React, Vue)

- Express : Web app Framework for Node (Server-side)

# Atomic Design Principles (Web Designing)

- Atom : smallest unit. eg. Button, Input field
- Molecules : combi of atoms. eg. Card, SearchBar (input with button)
- Organism : combi of molecules. eg. Form, Navigation Bar (NavLinks + SearchBar)
- Templates : combi of organism. eg. Forms
- Page : Single Page

# React Concepts

1. Thinking in React way. If it is reusable, make it a component.
2. Lifting-up the state; using functional props

# CRA Command

- npx create-react-app frontend
- npm start => localhost:3000 => index.html => index.js => App Component

# JSX

- Objects are NOT valid child
- Don't use JavaScript reserved keywords
- JSX must have only one root element

# Bootstrap

- cd frontend
- npm install bootstrap
- import "bootstrap/dist/css/bootstrap.min.css" in index.js

# Controlled vs Uncontrolled

- Controlled :
  > state is managed by React State variable
  > when instant feedback is needed (eg. Registration Form)
  > Component re-renders because React state is changed
- Uncontrolled :
  > state is managed by DOM
  > when the value on element is required without processing it immediately (eg. Login Form)
  > Component does not re-render for any change

# React Hooks

- useEffect() Flavours :

  > useEffect(cb) : cb executes when the initial rendering is completed; Also executes after every re-rendering of component.

  > useEffect(cb, []) / <componentDidMount> : cb ONLY executes when the initial rendering is completed

  > useEffect(cb, [Dependencies]) / <componentDidUpdate> : cb executes when the initial rendering is completed; Also executes when the mentioned dependency will change

  > useEffect(cb => cleanUpFn, [Dependencies]) / <componentWillUnmount> : cb executes when the initial rendering is completed; When the mentioned dependencies changed, cleanUpFn will executes before the cb executes. CleanUpFn will also executes when the component is about to destroy/unmount.

- useContext()

# Prop-Drilling : sending props unnecessarily to the child component

- Context API
- Component Composition

- useReducer()
  > Multiple state slices
  > Next state depends upon previous state
  > Complex state management logic

# React Component Optimization

- memo() : compares old vs new props
- useCallback()
- useMemo()

# Single Page App

- react-router-dom

# Redux Pattern

---

> npx create-react-app react-spa
> npm install react-router-dom bootstrap

---

# JSON Server installation

- npm install json-server@0.17.4 -g
- db.json
- json-server --watch db.json --port=3031
- http://localhost:3030/courses

---

- Case 01 - Loader (Pre-fetching data)
  Courses Link -> Loader -> CourseLoader -> courses -> CoursesPage -> Loaded on DOM

- Case 02 - useState / useEffect
  Course Link -> CoursePage -> Loaded on DOM -> useEffect() -> fetch -> courses data

---

# npm install react-router-dom

# React Router DOM Terminologies

- useParams : Route Parameter
- useSubmit : Programmatic form submission
- useNavigate : programmatic navigation
- useRouteError : Handle error occured in routes
- useLoaderData : Pre-fetch data
- useRouteLoaderData : Pre-fetch data from parent routes
- Route Configuration - errorElement, children, actions, loaders
- Route Components - Outlet, Link, NavLink, Form

---

react state : for one component
react context api : not very efficient

---

# Redux : Predictable state management container

# Redux Building Blocks

- Action : Object; {type : "", payload?: ""}
- Reducer: Pure function; (prevState, action) => newState
- Store : Single Object; { counter, result, todos, token }
  > getState()
  > dispatch()
  > subscribe()

# npm install react-redux @reduxjs/toolkit bootstrap
