// Spread (...)

// let arr = [1, 2, 3];

// let moreArray = [arr, 4, 5, 6];

// console.log(moreArray);

// let userOne = {
//   email: "john@test",
//   company: "Synechron",
// };

// let userTwo = {
//   ...userOne,
//   email: "jenny@test",
// };

// console.log(userTwo); // { email : "jenny@test",company: "Synechron" }

// Promise
const demofn = () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve({ message: "Data" });
    }, 3000);
  });
};

const consume = async () => {
  const response = await demofn();
  console.log(response);
};

consume();
