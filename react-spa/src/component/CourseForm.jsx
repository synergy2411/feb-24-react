import { useRef } from "react";
import { Form, useSubmit } from "react-router-dom";

function CourseForm({ course }) {
  const titleRef = useRef();
  const durationRef = useRef();
  const logoRef = useRef();

  const submit = useSubmit();

  const submitHandler = (e) => {
    e.preventDefault();
    let createdCourse = {
      title: titleRef.current.value,
      duration: durationRef.current.value,
      logo: logoRef.current.value,
    };
    if (course) {
      submit(createdCourse, {
        method: "PATCH",
        action: `/courses/${course.id}/edit`,
      });
    } else {
      submit(createdCourse, { method: "POST", action: "/courses/new" });
    }
  };
  return (
    <Form onSubmit={submitHandler}>
      {/* title */}
      <div className="form-floating mb-3">
        <input
          type="text"
          className="form-control"
          name="title"
          id="title"
          placeholder=""
          defaultValue={course ? course.title : ""}
          ref={titleRef}
        />
        <label htmlFor="title">Title</label>
      </div>
      {/* duration */}
      <div className="form-floating mb-3">
        <input
          type="number"
          className="form-control"
          name="duration"
          id="duration"
          placeholder=""
          defaultValue={course ? course.duration : ""}
          ref={durationRef}
        />
        <label htmlFor="duration">Duration</label>
      </div>
      {/* logo */}
      <div className="form-floating mb-3">
        <input
          type="text"
          className="form-control"
          name="logo"
          id="logo"
          placeholder=""
          defaultValue={course ? course.logo : ""}
          ref={logoRef}
        />
        <label htmlFor="logo">Logo</label>
      </div>
      {/* buttons */}
      <div className="row">
        <div className="col-6">
          <div className="d-grid">
            <button className="btn btn-secondary" type="submit">
              {course ? "Edit" : "Add"}
            </button>
          </div>
        </div>
        <div className="col-6">
          <div className="d-grid">
            <button className="btn btn-light">Cancel</button>
          </div>
        </div>
      </div>
    </Form>
  );
}

export default CourseForm;
