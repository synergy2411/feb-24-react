import classes from "./CourseItem.module.css";

function CourseItem({ course, onCourseSelect }) {
  return (
    <div className="col-6 mb-3">
      <div
        className={`card ${classes["clickable"]}`}
        onClick={() => onCourseSelect(course.id)}
      >
        <div className="card-header">
          <h5 className="text-center">{course.title.toUpperCase()}</h5>
        </div>
      </div>
    </div>
  );
}

export default CourseItem;
