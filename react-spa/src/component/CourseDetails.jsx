import { useNavigate, useSubmit } from "react-router-dom";

function CourseDetails({ course }) {
  const submit = useSubmit();
  const navigate = useNavigate();

  const deleteHandler = () => {
    const proceed = window.confirm(
      `Are you sure to delete course - ${course.id}`
    );
    if (proceed) {
      submit({}, { method: "DELETE", action: `/courses/${course.id}` });
    }
  };

  const editHandler = () => {
    navigate(`/courses/${course.id}/edit`);
  };
  return (
    <div className="card" style={{ width: "18rem" }}>
      <img className="card-img-top" src={course.logo} alt={course.title} />
      <div className="card-body text-center">
        <h4 className="card-title">{course.title.toUpperCase()}</h4>
        <p className="card-text">Duration : {course.duration}Hrs</p>
        <div className="row">
          <div className="col-6">
            <div className="d-grid">
              <button
                className="btn btn-outline-danger btn-sm"
                onClick={deleteHandler}
              >
                Delete
              </button>
            </div>
          </div>
          <div className="col-6">
            <div className="d-grid">
              <button
                className="btn btn-outline-success btn-sm"
                onClick={editHandler}
              >
                Edit
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CourseDetails;
