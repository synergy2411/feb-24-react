import { useNavigate, useRouteError } from "react-router-dom";
import MainNavigation from "../../component/MainNavigation/MainNavigation";

function ErrorPage() {
  const navigate = useNavigate();
  const { status, data } = useRouteError();

  let message = data.message || "Something went wrong";
  let code = status || "404";

  return (
    <div className="row text-center">
      <div className="col-8 offset-2">
        <MainNavigation />
        <div className="text-center mt-4">
          <h1>{message}</h1>
          <p>Status : {code}</p>
          <button className="btn btn-info" onClick={() => navigate("/")}>
            Home Page
          </button>
        </div>
      </div>
    </div>
  );
}

export default ErrorPage;
