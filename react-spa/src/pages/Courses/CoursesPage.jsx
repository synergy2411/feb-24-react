import { useLoaderData, useNavigate } from "react-router-dom";
import CourseItem from "../../component/CourseItem";

// http://localhost:3000/courses
function CoursesPage() {
  const courses = useLoaderData();
  const navigate = useNavigate();

  const onCourseSelect = (courseId) => {
    navigate(`/courses/${courseId}`);
  };

  return (
    <>
      <h1>Courses Page</h1>
      <div className="row mb-4">
        <div className="col-6 offset-3">
          <div className="d-grid">
            <button
              className="btn btn-primary"
              onClick={() => navigate("/courses/new")}
            >
              Add Course
            </button>
          </div>
        </div>
      </div>
      <div className="row">
        {courses.map((course) => (
          <CourseItem
            key={course.id}
            course={course}
            onCourseSelect={onCourseSelect}
          />
        ))}
      </div>
    </>
  );
}

export default CoursesPage;

export async function loader() {
  const response = await fetch("http://localhost:3030/courses");
  if (!response.ok) {
    throw new Error("Unable to fetch courses");
  }
  const courses = await response.json();
  return courses;
}
