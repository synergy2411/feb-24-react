import { json, redirect } from "react-router-dom";
import CourseForm from "../../component/CourseForm";

function NewCoursePage() {
  return (
    <>
      <h1>New Course Page</h1>
      <CourseForm />
    </>
  );
}

export default NewCoursePage;

export async function action({ request }) {
  const data = await request.formData();
  const newCourse = {
    title: data.get("title"),
    duration: Number(data.get("duration")),
    logo: data.get("logo"),
  };

  const response = await fetch("http://localhost:3030/courses", {
    method: request.method,
    body: JSON.stringify(newCourse),
    headers: {
      "Content-Type": "application/json",
    },
  });

  if (!response.ok) {
    throw json({ message: "Unable to create new course" }, { status: "402" });
  }
  return redirect("/courses");
}
