import { redirect, useRouteLoaderData, json } from "react-router-dom";
import CourseDetails from "../../component/CourseDetails";

function CourseDetailsPage() {
  // const course = useLoaderData();
  const course = useRouteLoaderData("course-detail-loader");

  return (
    <>
      <h1>Course Details Page</h1>
      <CourseDetails course={course} />
    </>
  );
}

export default CourseDetailsPage;

export async function loader({ params }) {
  const { courseId } = params;

  const response = await fetch(`http://localhost:3030/courses/${courseId}`);

  if (!response.ok) {
    throw json(
      { message: "Unable to fetch the course for - " + courseId },
      { status: 404 }
    );
  }

  const course = await response.json();

  return course;
}

export async function action({ request, params }) {
  const { courseId } = params;
  console.log("DELETE :", courseId);
  const response = await fetch(`http://localhost:3030/courses/${courseId}`, {
    method: request.method,
  });

  if (!response.ok) {
    throw json(
      { message: "Unable to delete the course for - " + courseId },
      { status: 404 }
    );
  }

  return redirect("/courses");
}
