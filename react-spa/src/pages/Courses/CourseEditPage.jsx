import { redirect, useRouteLoaderData, json } from "react-router-dom";
import CourseForm from "../../component/CourseForm";

function CourseEditPage() {
  const course = useRouteLoaderData("course-detail-loader");
  return (
    <>
      <h1>Course Edit Page</h1>
      <CourseForm course={course} />
    </>
  );
}

export default CourseEditPage;

export async function action({ request, params }) {
  const data = await request.formData();
  const { courseId } = params;
  const newCourse = {
    title: data.get("title"),
    duration: Number(data.get("duration")),
    logo: data.get("logo"),
  };

  const response = await fetch(`http://localhost:3030/courses/${courseId}`, {
    method: request.method,
    body: JSON.stringify(newCourse),
    headers: {
      "Content-Type": "application/json",
    },
  });

  if (!response.ok) {
    throw json({ message: "Unable to create new course" }, { status: "401" });
  }
  return redirect("/courses");
}
