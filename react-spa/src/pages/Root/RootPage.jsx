import { Outlet } from "react-router-dom";
import MainNavigation from "../../component/MainNavigation/MainNavigation";

// http://localhost:3000

function RootPage() {
  return (
    <div className="row text-center">
      <div className="col-8 offset-2">
        <MainNavigation />
        <Outlet />
      </div>
    </div>
  );
}

export default RootPage;
