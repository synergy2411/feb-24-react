import { createBrowserRouter, RouterProvider } from "react-router-dom";
import CourseDetailsPage, {
  loader as CourseDetailsLoader,
  action as CourseDetailsAction,
} from "./pages/Courses/CourseDetailsPage";
import CoursesPage, {
  loader as CoursesLoader,
} from "./pages/Courses/CoursesPage";
import NewCoursePage, {
  action as NewCourseAction,
} from "./pages/Courses/NewCoursePage";
import ErrorPage from "./pages/Error/ErrorPage";
import HomePage from "./pages/Home/HomePage";
import RootPage from "./pages/Root/RootPage";
import CourseEditPage, {
  action as CourseEditAction,
} from "./pages/Courses/CourseEditPage";

// http://localhost:3000

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootPage />,
    errorElement: <ErrorPage />,
    children: [
      { index: true, element: <HomePage /> },
      {
        path: "/courses",
        element: <CoursesPage />,
        loader: CoursesLoader,
      },
      {
        path: "/courses/:courseId",
        id: "course-detail-loader",
        loader: CourseDetailsLoader,
        action: CourseDetailsAction,
        children: [
          {
            index: true,
            element: <CourseDetailsPage />,
          },
          {
            path: "edit",
            element: <CourseEditPage />,
            action: CourseEditAction,
          },
        ],
      },
      {
        path: "/courses/new",
        element: <NewCoursePage />,
        action: NewCourseAction,
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router}></RouterProvider>;
}

export default App;
